function getObj(data) {
  var objs = {},i,j,len,lines = data.split(/[\r\n]+/), obj, line, a, face,
      vertex, totalVertices = 1, lastObjNumVertices = 1;
  for (i = 0, len = lines.length; i < len; i++) {
    line = lines[i];
    // the first letter of each line describes what type of object is on that
    // line (o for object, v for vertex, f for face)
    if (line[0] === 'o') {
      obj = objs[line.slice(2).toString()] = {vertices:[],faces:[]};
      lastObjNumVertices = totalVertices;
    } else if (line[0] === 'v') {
      // add this vertex to our current object as an array 
      // of 3 numbers ([x, y, z])
      a = line.slice(2).split(' ');
      vertex = [];
      for (j = 0; j < a.length; j++) vertex.push(Number(a[j]));
      obj.vertices.push(vertex);
      totalVertices++;
    } else if (line[0] === 'f') {
      // add this face to our current object as an array of vertex indices
      a = line.slice(2).split(' ');
      face = [];
      for (j = 0; j < a.length; j++) {
        face.push(Number(a[j])-lastObjNumVertices);
      }
      obj.faces.push(face);
    }
  }
  return objs;
}
