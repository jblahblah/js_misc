function Particle(x,y,z,mass,damping) {
  this.pos = new Vec3(x,y,z);
  this.vel = new Vec3(0,0,0);
  this.accel = new Vec3(0,0,0);
  this.damping = damping;
  this.invmass = 1/mass;
  this.mass = mass;
  this.forceAccum = new Vec3(0,0,0);
}

Particle.prototype.clearAccumulator = function() {
  this.forceAccum.clear();
}

Particle.prototype.setMass = function(val) {
  this.mass = val;
  this.invmass = 1/val;
}

Particle.prototype.tick = function(dt) {
  if (this.invmass > 0 && dt > 0) {
    this.pos.addScaledVec(this.vel, dt);

    var a = this.accel.copy();

    a.addScaledVec(this.forceAccum, this.invmass);

    this.vel.addScaledVec(a, dt);

    // impose drag
    this.vel.mult(Math.pow(this.damping,dt));

    // clear the forces
    this.clearAccumulator();
  }
}

Particle.prototype.draw = function(ctx) {
  var x = Math.round(this.pos.x),
      y = Math.round(this.pos.y),
      circ = Math.PI*2;
  ctx.beginPath();
  ctx.arc(x, y, 5, circ, false);
  ctx.fillStyle = Particle.color;
  ctx.fill();
  ctx.closePath();

  ctx.beginPath();
  ctx.arc(x, y, 1, circ, false);
  ctx.fillStyle = '#333';
  ctx.fill();
  ctx.closePath();
}

Particle.prototype.addForce = function(f) {
  this.forceAccum.addV(f);
}

Particle.color = '#bbb';
