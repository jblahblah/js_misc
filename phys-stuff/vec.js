function Vec3(x,y,z) {
  if (x === undefined) x = 0;
  if (y === undefined) y = 0;
  if (z === undefined) z = 0;
  this.x = x;
  this.y = y;
  this.z = z;
}

Vec3.prototype.set = function(x,y,z) {
  if (x === undefined) x = 0;
  if (y === undefined) y = 0;
  if (z === undefined) z = 0;
  this.x = x;
  this.y = y;
  this.z = z;
}

Vec3.prototype.clear = function() {
  this.x = 0;
  this.y = 0;
  this.z = 0;
}

Vec3.prototype.invert = function () {
  this.x = -this.x;
  this.y = -this.y;
  this.z = -this.z;
}

Vec3.prototype.magnitude = function () {
  return Math.sqrt(this.x*this.x+this.y*this.y+this.z*this.z);
}

Vec3.prototype.squareMagnitude = function () {
  return this.x*this.x+this.y*this.y+this.z*this.z;
}

Vec3.prototype.normalize = function () {
  var mag = this.magnitude();
  // we cannot normalize the zero vector
  if (mag > 0) {
    this.x = this.x/mag;
    this.y = this.y/mag;
    this.z = this.z/mag;
  }
  return this;
}

Vec3.prototype.add = function (val) {
  this.x += val;
  this.y += val;
  this.z += val;
  return this;
}

Vec3.prototype.sub = function (val) {
  this.x -= val;
  this.y -= val;
  this.z -= val;
  return this;
}

Vec3.prototype.addV = function (v) {
  this.x += v.x;
  this.y += v.y;
  this.z += v.z;
  return this;
}

Vec3.prototype.subV = function (v) {
  this.x -= v.x;
  this.y -= v.y;
  this.z -= v.z;
  return this;
}

Vec3.prototype.mult = function (val) {
  this.x *= val;
  this.y *= val;
  this.z *= val;
  return this;
}

Vec3.prototype.div = function (val) {
  this.x /= val;
  this.y /= val;
  this.z /= val;
  return this;
}

Vec3.prototype.copy = function() {
  return new Vec3(this.x, this.y, this.z);
}

Vec3.prototype.multCopy = function (val) {
  return new Vec3(this.x * val, this.y * val, this.z * val);
}

Vec3.prototype.addCopy = function (val) {
  return new Vec3(this.x + val, this.y + val, this.z + val);
}

Vec3.prototype.subCopy = function (val) {
  return new Vec3(this.x - val, this.y - val, this.z - val);
}

Vec3.prototype.divCopy = function (val) {
  return new Vec3(this.x / val, this.y / val, this.z / val);
}


Vec3.prototype.addCopyV = function (v) {
  return new Vec3(this.x + v.x, this.y + v.y, this.z + v.z);
}

Vec3.prototype.subCopyV = function (v) {
  return new Vec3(this.x - v.x, this.y - v.y, this.z - v.z);
}

Vec3.prototype.divCopyV = function (v) {
  return new Vec3(this.x / v.x, this.y / v.y, this.z / v.z);
}

/**
* Adds given vector to this, scaled by the given amount
*/
Vec3.prototype.addScaledVec = function (vec, scale) {
  this.x += vec.x * scale;
  this.y += vec.y * scale;
  this.z += vec.z * scale;
  return this;
}

Vec3.prototype.componentProd = function (vec) {
  this.x *= vec.x;
  this.y *= vec.y;
  this.z *= vec.z;
  return this;
}

Vec3.prototype.componentProdCopy = function (vec) {
  return new Vec3(this.x * vec.x, this.y * vec.y, this.z * vec.z);
}

Vec3.prototype.dotProd = function (vec) {
  return this.x * vec.x + this.y * vec.y + this.z * vec.z;
}

Vec3.prototype.crossProd = function (vec) {
  this.x = this.y * vec.z - vec.z * this.y;
  this.y = this.z * vec.x - vec.x * this.z;
  this.z = this.x * vec.y - vec.y * this.x;
  return this;
}

Vec3.prototype.crossProdCopy = function (vec) {
  return new Vec3(
    this.y * vec.z - vec.z * this.y,
    this.z * vec.x - vec.x * this.z,
    this.x * vec.y - vec.y * this.x
  );
}

Vec3.MakeOrthonormalBasis = function (a,b,c) {
}

Vec3.prototype.toString = function() {
  return '('+this.x+','+this.y+','+this.z+')';
}

Vec3.prototype.draw = function(ctx) {
  var x = Math.round(this.x),
      y = Math.round(this.y),
      circ = Math.PI*2;
  ctx.beginPath();
  ctx.arc(x, y, 3, circ, false);
  ctx.fillStyle = '#1bbf55';
  ctx.fill();
  ctx.closePath();

  ctx.beginPath();
  ctx.arc(x, y, 1, circ, false);
  ctx.fillStyle = '#333';
  ctx.fill();
  ctx.closePath();
}

