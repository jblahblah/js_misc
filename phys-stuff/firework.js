function Firework(x,y) {
  this.particle = new Particle(x,y,0;
  this.type = Firework.TYPE_DEFAULT;
  this.age = 0.0;
}

Firework.TYPE_DEFAULT = 0;

Particle.prototype.tick = function(dt) {
  this.age += dt;
  this.particle.tick(dt);
}

/**
 * @param c a CanvasRenderingContext2D object
 */
Particle.prototype.draw = function(c) {
  
}
