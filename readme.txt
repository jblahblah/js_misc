# Javascript Miscellany
## What is this?
A collection of random Javascript (and HTML/CSS) stuff that I'm playing around with.

## Why?
I don't want to lose small tests and stuff, I want to keep them all in one place.
I also want to explore Javascript, HTML, and CSS more, creating demos and trying out
weird things.

