fun! MinimizeEasyJSC()
  :silent! %s/window/#/g
  :silent! %s/mageData/@/g
  :silent! %s/function/%/g
  :silent! %s/toString(16)/^/g
  :silent! %s/+Number(i\[/!/g
  :silent! %s/attachEvent/~/g
  :silent! %s/addEventListener/`/g
  :silent! %s/onkeyup/¥/g
  :silent! %s/onkeydown/¤/g
  :silent! %s/keyCode/ß/g
  :silent! %s/document\./£/g
endf
