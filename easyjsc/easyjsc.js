"use strict";

/**
 * TODO:
 * - fix: clear() not always working
 * - bugs when passing false as second arg to d
 */

function i(W,h,Fps,bg,useImageData) {
  var w=window,o=document,n=Number,u=Date.now;w.m=Math;
  w.onload=function(){
    w.sw = W||700;
    w.sh = h||700;
    w.fps = Fps||60;
    // frame
    w.f = 0;
    w.dt = 0;
    // previous frame's timestamp
    w.pt = u();
    w.keys = {};
    // make a dictionary of ASCII values of keys, for ez access
    w.keyCodes = {};
    for(var k=32;k<127;k++)w.keyCodes[String.fromCharCode(k)]=k;
    o.body.style.backgroundColor = bg||'#eee';
    w.v = o.createElement('canvas');
    v.setAttribute('width',(W||700)+'px');
    v.setAttribute('height',(h||700)+'px');
    o.body.appendChild(v);
    w.c = v.getContext('2d');
    w.addEventListener('keydown',jonkeydown);
    w.addEventListener('keyup',jonkeyup);
    //w.onkeydown = jonkeydown;
    //w.onkeyup = jonkeyup;

    var draw = function() {
        w.dt = (u()-pt)/1000;
        if (useImageData)  {
            w.id = c.getImageData(0,0, sw, sh);
            w.data = id.data;
            drawFn();
            c.putImageData(id,0,0);
        } else {
            drawFn();
        }
        f++;
        pt = u();
    }

    w.save = function() {
      id = c.getImageData(0,0, sw, sh);
    }

    w.p = function(x,y,r,g,b,a) {
      var index = (x + y * id.width) * 4;
      data[index+0] = r;
      data[index+1] = g;
      data[index+2] = b;
      data[index+3] = a;
    }
    w.g = function(x,y) {
      var id = c.getImageData(x,y,1,1);
      var data = id.data;
      return '#' +
        n(data[0]).toString(16) +
        n(data[1]).toString(16) +
        n(data[2]).toString(16) +
        n(data[3]).toString(16);
    }
    w.on = function(x,y) { p(x,y,0,0,0,255); };
    w.off = function(x,y) { p(x,y,255,255,255,255); };
    w.t = w.hasOwnProperty('console') ? console.log : alert;

    if (tickEveryFrame) {
      setInterval(draw,1000/fps);
    } else {
      draw();
    }
  }
}

function jonkeydown (e) {
  keys[e.keyCode] = true;
}

function jonkeyup (e) {
  keys[e.keyCode] = false;
}

function dist(x,y,x1,y1){return m.sqrt(m.pow(x-x1)+m.pow(y-y1))}

function d(f, tick){
  window.drawFn = f;
  window.tickEveryFrame = tick;
}

function clear(){
  c.clearRect(0, 0, sw, sh);
}

//function i(W,h,Fps,bg){var w=window,o=document,n=Number,u=Date.now;w.m=Math;w.onload=function(){w.sw=W||700;w.sh=h||700;w.fps=Fps||60;w.f=0;w.dt=0;w.pt=u();w.keys={};w.keyCodes={};for(var k=32;k<127;k++)w.keyCodes[String.fromCharCode(k)]=k;o.body.style.backgroundColor=bg||'#eee';w.v=o.createElement('canvas');v.setAttribute('width',(W||700)+'px');v.setAttribute('height',(h||700)+'px');o.body.appendChild(v);w.c=v.getContext('2d');w.onkeydown=onkeydown;w.onkeyup=onkeyup;var draw=function(){w.dt=(u()-pt)/1000;w.id=c.getImageData(0,0,sw,sh);w.data=id.data;drawFn();c.putImageData(id,0,0);f++;pt=u();}if(tickEveryFrame){setInterval(draw,1000/fps);}else{draw();}w.save=function(){id=c.getImageData(0,0,sw,sh);}w.p=function(x,y,r,g,b,a){var index=(x+y*id.width)*4;data[index+0]=r;data[index+1]=g;data[index+2]=b;data[index+3]=a;}w.g=function(x,y){var id=c.getImageData(x,y,1,1);var data=id.data;return'#'+n(data[0]).toString(16)+n(data[1]).toString(16)+n(data[2]).toString(16)+n(data[3]).toString(16);}w.on=function(x,y){p(x,y,0,0,0,255);};w.off=function(x,y){p(x,y,255,255,255,255);};w.t=w.hasOwnProperty('console')?console.log:alert;}}function onkeydown(e){if(keys.length>e.keyCode&&keys[e.keyCode]>0){keys[e.keyCode]++;}else{keys[e.keyCode]=1;}}function onkeyup(e){keys[e.keyCode]=0;}function dist(x,y,x1,y1){returnm.sqrt(m.pow(x-x1)+m.pow(y-y1))}function d(f,tick){window.drawFn=f;window.tickEveryFrame=tick;}function clear(){c.clearRect(0,0,sw,sh);}

