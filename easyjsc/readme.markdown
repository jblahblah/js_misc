# easyjsc: easy javascript canvas
## What is this?
A tiny library that aims to be the quickest way to start drawing stuff with
Javascript, in a \<canvas\> tag.

## How do I use it?
See example.html for usage. After that, copy template.html to a new file, and edit that.

## to do
- draw images, or sprites from spritesheet
- better fallback for console.log than alert (append to div)
- use requestAnimationFrame?

## api
* d(fn,tickEveryFrame) - set the draw/update function
  * fn (function) - the function to draw/update with
  * tickEveryFrame (bool) - if true, easyjsc will call fn fps times per
    second. if false, your draw function will only be called once.
* i(w,h,fps,bg) - initialize easyjsc and its canvas
  * w (int) - the width of the canvas to be made
  * h (int) - the height of the canvas to be made
  * fps (int) - the number of times to call your draw function, per second
  * bg (string) - the background color of the canvas and html page
* on(x,y) - draw a black pixel at (x,y)
* off(x,y) - draw a transparent pixel at (x,y)
* p(x,y,r,g,b,a); - set the color of the pixel at (x,y)
* g(x,y) - get the color of the pixel at (x,y)
* clear() - clear the canvas
* dist(x0,y0,x1,y1) - get the distance between the points (x0,y0) and (x1,y1)
* save() - prevent the contents of the canvas from redrawing
* t(...) - trace/print out a debug message
* c - the canvas 2D context
* dt - delta time since last frame
* f - frame #
* fps - frames per second
* keyCodes - dictionary of key codes
* keys - dictionary of key states
* m - pointer to Math
* sh - screen height
* sw - screen width
* v - the canvas
