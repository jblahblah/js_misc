<script src="easyjsc_minimal.js"></script>
<script>
  // your code goes here.
  setDraw(function() {
    // radius
    var r = 75,
    // border
    b = 15,
    // tunnel height
    th = r,
    // bounding rect
    x = 50, y = 50, w = 400, h = r, 
    fg = '#00ee33', bg = '#220066';

    c.fillStyle = fg;
    c.fillRect(x,y,w,h);

    c.fillStyle = bg;

    // cut out left tunnel edge
    c.beginPath();
    c.arc(x+r+b, y, r, Math.pi, 1/2*Math.pi, true);
    c.lineTo(x+r+b,y);
    c.fill();
    c.closePath();

    // cut out right tunnel edge
    c.beginPath();
    c.arc(x+w-b-r, y+r, r, 3*Math.pi/2, 0);
    c.lineTo(x+w-b-r,y+r);
    c.fill();
    c.closePath();

    // cut out rest of tunnel
    c.fillRect(x+r+b, y, w - 2*(r+b), r);

    // fill in top edge
    c.beginPath();
    c.arc(x+r+b, y, r, Math.pi, 1/2*Math.pi, true);
    c.lineTo(x+r+b,y);
    c.fill();
    c.closePath();

    // fill in bottom edge
    c.beginPath();
    c.arc(x+w-b-r, y+r, r, 3*Math.pi/2, 0);
    c.lineTo(x+w-b-r,y+r);
    c.fill();
    c.closePath();

    save();
  }, false);
</script>
