var DrawLib;
(function (DrawLib) {
  DrawLib.makeCanvas = function(w,h) {
    var e = document.createElement('canvas');
    e.setAttribute('width',(w||700)+'px');
    e.setAttribute('height',(h||700)+'px');
    document.body.appendChild(e);
    return e.getContext('2d');
  };

  DrawLib.Vec2d = function(x,y) {
    this.x = x;
    this.y = y;
  };

  DrawLib.Color = function(r,g,b,a) {
    this.x = x;
    this.y = y;
  };

  DrawLib.drawline = function(buf,col,x0,y0,x1,y1) {
    var dx = Math.abs(x1-x0), incx = (x1 > x0) ? 1 : -1,
    dy = -Math.abs(y1-y0), incy = (y1 > y0) ? 1 : -1,
    error = dx+dy;

    while (true) {
        var i = ((x0 >> 0) + (y0 >> 0) * this.w) * 4;
        this.buf[i] = col.r * 255;
        this.buf[i+1] = col.g * 255;
        this.buf[i+2] = col.b * 255;
        this.buf[i+3] = col.a * 255;
        a++;
      // if we instead do "while (x0==x1 && y0==y1)", the DrawLib.breaks = function.  why?
      if (x0==x1 && y0==y1) break;
      var error2 = 2*error;
      if (error2 >= dy) error += dy, x0 += incx; /* e_xy+e_x > 0 */
      if (error2 <= dx) error += dx, y0 += incy; /* e_xy+e_y < 0 */
    }
  };

  DrawLib.trifill = function(buf,col,a,b,c) {
    var t;
    // sort vertices by y value
    if (a.y > b.y) { t = a; a = b; b = t; }
    if (a.y > c.y) { t = a; a = c; c = t; }
    if (b.y > c.y) { t = b; b = c; c = t; }

    if (a.y == b.y) {
      // fill triangle with a flat top
      // we pass the points in a different order, because 
      // points a and b have the same y-value.
      fillFlatTri(c.x, c.y, a.x, a.y, b.x, b.y);
    } else if (b.y == c.y) {
      // fill triangle with a flat bottom
      fillFlatTri(a.x, a.y, b.x, b.y, c.x, c.y);
    } else {
      // general case: split triangle in two, with a flat top and bottom
        var d = new Vec2d(a.x + ((b.y - a.y)*((c.x-a.x)/(c.y-a.y))),b.y);
        //console.log(d.x,d.y);
        fillFlatTri(buf,col,a.x, a.y, b.x, b.y, c.x, c.y);
        fillFlatTri(buf,col,c.x, c.y, b.x, b.y, d.x, d.y);

        /*
        * p(d.x,d.y,255,0,255,255);
        * p(d.x+1,d.y,255,0,255,255);
        * p(d.x+1,d.y+1,255,0,255,255);
        * p(d.x,d.y+1,255,0,255,255);

        */  
    }
    /*
    * 
    *   p(a.x,a.y,255,0,0,255);
    *   p(b.x,b.y,0,255,0,255);
    *   p(c.x,c.y,0,0,255,255);
    *   p(a.x+1,a.y,255,0,0,255);
    *   p(b.x+1,b.y,0,255,0,255);
    *   p(c.x+1,c.y,0,0,255,255);
    *   p(a.x+1,a.y+1,255,0,0,255);
    *   p(b.x+1,b.y+1,0,255,0,255);
    *   p(c.x+1,c.y+1,0,0,255,255);
    *   p(a.x,a.y+1,255,0,0,255);
    *   p(b.x,b.y+1,0,255,0,255);
    *   p(c.x,c.y+1,0,0,255,255);
    * 
    */
  };

  /** 
  * Draws a triangle with a flat horizontal edge.
  * This requires that v1 == y1 and y0 != y1.
  */
  DrawLib.fillFlatTri = function(buf,col,x0,y0,x1,y1,u1,v1) {
    // (x0,y0) -> (y1,y1)
    var dx = Math.abs(x1-x0), incx = (x1 > x0) ? 1 : -1,
    dy = -Math.abs(y1-y0), incy = (y1 > y0) ? 1 : -1,
    errorXY = dx+dy,
    // (u0,v0) -> (u1,v1) vars
    u0 = x0, v0 = y0,
    du = Math.abs(u1-u0), incu = (u1 > u0) ? 1 : -1,
    dv = -Math.abs(v1-v0), incv = (v1 > v0) ? 1 : -1,
    errorUV = du+dv;

    //console.log(x0,y0,x1,y1,u0,v0,u1,v1);

    while (true) {
      if (x0==x1 && y0==y1) break;

      var errorXY2 = 2*errorXY;
      if (errorXY2 >= dy) errorXY += dy, x0 += incx;
      if (errorXY2 <= dx) errorXY += dx, y0 += incy;

      while (v0 != y0) {
        var errorUV2 = 2*errorUV;
        if (errorUV2 >= dv) errorUV += dv, u0 += incu;
        if (errorUV2 <= du) errorUV += du, v0 += incv;
      }

      var a = x0, b = u0;
      if (u0 < x0) { a = u0; b = x0; }

      //console.log(y0,a,b);
      //console.log(y0,y1,v0,v1);

      while (a < b)  {
        //p(a,y0,255,255,255,50);
        var i = ((a >> 0) + (y0 >> 0) * this.w) * 4;
        this.buf[i] = col.r * 255;
        this.buf[i+1] = col.g * 255;
        this.buf[i+2] = col.b * 255;
        this.buf[i+3] = col.a * 255;
        a++;
      }
    }
  };
})(DrawLib || (DrawLib = {}));

