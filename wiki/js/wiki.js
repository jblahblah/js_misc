/* vim: se ts=4 sts=4 sw=4: */
/*jslint browser: true, devel: true, regexp: true, indent: 4, maxlen: 80 */
// we set showPageByID as a global, even though it is defined below, to avoid
// the benign 'function used before it's defined' jslint warning.
/*global Showdown: false, showPageByID: false */
// core lib
var wiki = (function () {
    "use strict";
    var rendertime,
        p = console.log || alert,
        // cached pages, as plain text
        pages = [],
        titles = [],
        curPage,
        mkd = new Showdown.converter({
            extensions: ['github', 'table']
        }),
        // shorthands for oft-used selectors
        c,
        s,
        n,
        t,
        invalidURL = /[^a-zA-Z0-9-._~:\/?#\[\]@!$&'()*+,;=]/;

    function makeElem(tag, body, attrs) {
        var elem = document.createElement(tag),
            n;
        elem.innerHTML = body || '';
        for (n in attrs) {
            if (attrs.hasOwnProperty(n)) {
                elem.setAttribute(n, attrs[n]);
            }
        }
        return elem;
    }

    function wrap(txt, tag, id, classes) {
        var openTag = tag;
        if (id) {
            openTag += ' id="' + id + '"';
        }
        if (classes) {
            openTag += ' class="' + classes + '"';
        }
        return '<' + openTag + '>' + txt + '</' + tag + '>';
    }

    function hasClass(elem, className) {
        return elem.className.match(new RegExp('\\b' + className + '\\b'));
    }

    function removeClass(elem, className) {
        var rx = new RegExp('\\b' + className + '\\b');
        elem.className = elem.className.replace(rx, '');
    }

    function addClass(elem, className) {
        if (!hasClass(elem, className)) {
            elem.className += ' ' + className;
        }
    }

    function foreach(obj, fn) {
        var i;
        for (i in obj) {
            if (obj.hasOwnProperty(i)) {
                fn(obj[i]);
            }
        }
    }

    // does a shallow search for children of parentElem with the given class
    c = function (parentElem, className) {
        var i, len, elems = [];
        for (i = 0, len = parentElem.children.length; i < len; i += 1) {
            if (hasClass(parentElem.children[i], className)) {
                elems.push(parentElem.children[i]);
            }
        }
        return elems;
    };
    s = function (y) {
        return document.getElementById(y);
    };
    n = function (y) {
        return document.getElementsByName(y);
    };
    t = function (y) {
        return document.getElementsByTagName(y);
    };

    function getTitle(str) {
        var i, len, title, words, firstLine;
        if (str && str.length) {
            firstLine = str.split(/[\r\n]/)[0];
            // remove any #'s
            firstLine = firstLine.replace('#', '');
            // convert "page name" to "pageName"
            words = firstLine.split(/\s+/);
            title = '';
            for (i = 0, len = words.length; i < len; i += 1) {
                title += words[i].charAt(0).toUpperCase() + words[i].slice(1);
            }
            //title = encodeURIComponent(title);
            // remove any punctuation

            title = title.replace(invalidURL, '');
        }
        return title;
    }

    function addtoc(pages, n) {
        var entry,
            hash,
            heading,
            headings = [],
            hs,
            i = 7,
            j,
            len,
            level = 1,
            link,
            pageTitle,
            parentToc = s('toc'),
            sectionTitle,
            toc,
            ul,
            uls = [];

        pageTitle = titles[curPage];

        // hide all existing toc elements
        foreach(parentToc.children, function (elem) {
            addClass(elem, 'hide');
        });

        // simply display toc and hide all others if toc for this
        // section already exists.
        toc = s('tocPage' + n);
        if (toc) {
            removeClass(toc, 'hide');
            return;
        }

        // otherwise, make and add our toc
        while (i > 0) {
            i -= 1;
            hs = t('h' + i);
            for (j = 0, len = hs.length; j < len; j += 1) {
                // HACK to get headings in order
                hs[j].setAttribute('class', 'heading');
            }
        }
        // HACK to get headings in order
        headings = c(s('page' + n), 'heading');

        // TODO: make toc a well-formed ul
        toc = makeElem('ul', '', {
            id: 'tocPage' + n
        });
        parentToc.appendChild(toc);
        for (i = 0, len = headings.length; i < len; i += 1) {
            heading = headings[i];
            level = parseInt(heading.tagName.charAt(1), 10);
            sectionTitle = getTitle(heading.innerHTML);
            hash = pageTitle + '#' + sectionTitle;
            heading.parentNode.insertBefore(makeElem('a', '', {
                name: hash
            }), heading);
            link = makeElem('a', heading.innerHTML, {
                href: '#' + hash
            });
            entry = makeElem('li', '');
            entry.appendChild(link);
            ul = makeElem('ul');
            entry.appendChild(ul);
            uls[level - 1] = ul;

            if (level === 1 || !uls[level - 2]) {
                toc.appendChild(entry);
            } else {
                uls[level - 2].appendChild(entry);
            }
        }
    }

    function addPagesList(pages) {
        var elem, html, i, len, div, ul, li, a;

        // make our container div
        div = s('pageslist');
        div.appendChild(ul = makeElem('ul'));

        for (i = 0, len = titles.length; i < len; i += 1) {
            a = makeElem('a', titles[i], {
                href: '#' + titles[i],
                onclick: 'wiki.onClickLink(event)'
            });
            li = makeElem('li', '');
            li.appendChild(a);
            ul.appendChild(li);
        }
    }

    function showPageByTitle(title) {
        var i, len;
        for (i = 0, len = pages.length; i < len; i += 1) {
            if (titles[i] === title) {
                showPageByID(i);
                document.location.hash = title;
            }
        }
    }

    function onClickLink(e) {
        var title = e.target.href.split('#')[1];
        if (title) {
            showPageByTitle(title);
        }
    }

    function addWikiLinks(elem) {
        var as = elem.getElementsByTagName('a'),
            a,
            i,
            len;
        for (i = 0, len = as.length; i < len; i += 1) {
            a = as[i];
            // only process links, not normal anchors
            if (a.href) {
                a.onclick = onClickLink;
            }
        }
    }

    function showPageByID(n) {
        var elem, html, txt, i, len, rendertime;
        if (!pages || pages.length <= n) {
            return;
        }

        curPage = n;
        document.title = titles[curPage];
        rendertime = Date.now();

        elem = s('page' + n);
        if (elem) {
            // show this page, if it already exists
            removeClass(elem, 'hide');
            console.log('showing cached', '#page' + n);
        } else {
            // else, render this page and show it
            // replace plain text with markdown rendered html of first page
            html = mkd.makeHtml(pages[n]);
            s('container').innerHTML +=
                wrap(html, 'div', 'page' + n, 'content');
        }

        // hide all other pages
        for (i = 0, len = pages.length; i < len; i += 1) {
            if (i !== n) {
                elem = s('page' + i);
                if (elem) {
                    addClass(elem, 'hide');
                }
            }
        }

        // add table of contents
        addtoc(pages, n);

        // add correct onclick event to links
        // (this is here rather than above because for some reason when
        // a page is hiddden its onclick events are removed).
        addWikiLinks(s('page' + n));

        rendertime = Date.now() - rendertime;
        // display elapsed time rendering
        s('rendertime').innerHTML = 'page rendered in ' + rendertime + ' ms';
    }

    // "/path/to/file.ext" -> "file"
    function basename(filepath) {
        var split,
            filename;
        // split by directory separator
        split = filepath.split(/[\/\\]/);
        // get last part of path (the full file name)
        filename = split[split.length - 1];
        // get filename before extension
        return filename.split('.')[0];
    }

    function render() {
        var body,
            end,
            html,
            h1,
            len,
            i,
            iframes,
            iframe,
            hash,
            subheading,
            match;

        // get ALL markdown texts
        iframes = t('iframe');
        for (i = 0, len = iframes.length; i < len; i += 1) {
            try {
                iframe = iframes[i].contentWindow.document.body.innerHTML;
                // get rid of any html opening and closing tags
                // added to the iframe by the browser
                iframe = iframe.replace(/^(<\/?\w+(\s*\w+="\w+")*>)+/, '');
                iframe = iframe.replace(/(<\/?\w+(\s*\w+="\w+")*>)+$/, '');
                pages.push(iframe);
                titles.push(basename(iframes[i].src));
            } catch (e) {
                // we'll hit this if an <iframe> points to a bad file
                // output an error if a file cannot be accessed?
            }
        }

        hash = document.location.hash;
        if (hash) {
            hash = hash.slice(1);
            // get rid of subheading, if there is one
            match = hash.match(/#/);
            if (match) {
                subheading = hash.slice(match.index + 1);
                hash = hash.slice(0, match.index);
            }
            showPageByTitle(hash);
            if (subheading) {
                window.location = '#' + hash + '#' + subheading;
            }
        } else {
            // show the first page
            showPageByID(0);
        }

        addPagesList();

        M.parseMath(document.body);
    }

    return {
        'render': render,
        's': s,
        't': t,
        'c': c,
        'n': n,
        'makeElem': makeElem,
        'showPageByID': showPageByID,
        // FIXME: aint work, make this a prototype instead
        // of a list of fns, pls
        'rendertime': rendertime,
        'txtPages': pages,
        'onClickLink': onClickLink
    };
}());

window.onload = function () {
    "use strict";
    // initialize
    wiki.render();
};
