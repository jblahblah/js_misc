#!/bin/bash
# this script will compile wiki.html into a single HTML file with
# js and css included.
# if you give this script a directory as an argument, it will copy a wiki.html
# file to that dir, add any text files there, and open up a web browser.
wikifile=bin/wiki.html

function templatize() {
  match=$1
  file=$2
  tag=$3
  wiki=$4
  sed -i "s@${match}@<${tag}>\n</${tag}>@" $wiki
  sed -i "/<${tag}>/r ${file}" $wiki
}

cd $(dirname $0)
mkdir -p bin
cp template/template.html $wikifile
# add css file(s) in one <style> tag
cat css/wiki.css js/mathscribe/jqmath-0.4.0.css > cssfiles
templatize "<!--styles-->" "cssfiles" style $wikifile
# add js file(s) in one <style> tag
cat js/showdown.min.js js/wiki.js js/mathscribe/jquery-1.10.1.min.js js/mathscribe/jqmath-etc-0.4.0.min.js > jsfiles
templatize "<!--scripts-->" "jsfiles" script $wikifile
rm cssfiles jsfiles

if [ -z $1 ]
then
  # this isn't necessary, but makes bin/wiki.html work just like wiki.html
  # right out the box
  cp -r txt bin
else
  textfilesdir=$1
  # TODO: warn and confirm overwrite if wiki.html already exists
  cp $wikifile $textfilesdir
  wikifile=wiki.html
  cd $textfilesdir
  iframes=""
  # FIXME: will break if there are spaces in filenames
  for textfile in $(ls *.txt); do
    iframes="${iframes}<iframe class=\"txt\" src=\"$textfile\"><\/iframe>\n"
  done
  sed -i "s@<!--files-->@${iframes}@" $wikifile
  echo $(pwd)
  echo $wikifile
fi
