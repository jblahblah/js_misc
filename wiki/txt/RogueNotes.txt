# Rogue 5.4.4 strategy guide

## random tips
- no healing can take place when fighting

## monsters

| Name            |Carry   |Flag             |str   |xp     |lvl   |amr   |hpt   |dmg|
| ================================================================================================|
| aquator         |0        |mean             |10    |20     |5     |2     |1     |0d0+0d0 _(0)_ |
| bat             |0        |fly              |10    |1      |1     |3     |1     |1d2         |
| centaur         |15       |0                |10    |17     |4     |4     |1     |1d2+1d5+1d5 |
| dragon          |100      |mean             |10    |5000   |10    |-1    |1     |1d8+1d8+3d10|
| emu             |0        |mean             |10    |2      |1     |7     |1     |1d2         |
| venus_flytrap   |0        |mean             |10    |80     |8     |3     |1     |0d0         |
| griffin         |20       |mean,fly,regen   |10    |2000   |13    |2     |1     |4d3+3d5     |
| hobgoblin       |0        |mean             |10    |3      |1     |5     |1     |1d8         |
| ice_monster     |0        |0                |10    |5      |1     |9     |1     |0d0         |
| jabberwock      |70       |0                |10    |3000   |15    |6     |1     |2d12+2d4    |
| kestrel         |0        |mean,fly         |10    |1      |1     |7     |1     |1d4         |
| leprechaun      |0        |0                |10    |10     |3     |8     |1     |1d1         |
| medusa          |40       |mean             |10    |200    |8     |2     |1     |3d4+3d4+2d5 |
| nymph           |100      |0                |10    |37     |3     |9     |1     |0d0         |
| orc             |15       |greed            |10    |5      |1     |6     |1     |1d8         |
| phantom         |0        |invis            |10    |120    |8     |3     |1     |4d4         |
| quagga          |0        |mean             |10    |15     |3     |3     |1     |1d5+1d5     |
| rattlesnake     |0        |mean             |10    |9      |2     |3     |1     |1d6         |
| snake           |0        |mean             |10    |2      |1     |5     |1     |1d3         |
| troll           |50       |regen,mean       |10    |120    |6     |4     |1     |1d8+1d8+2d6 |
| black_unicorn   |0        |mean             |10    |190    |7     |-2    |1     |1d9+1d9+2d9 |
| vampire         |20       |regen,mean       |10    |350    |8     |1     |1     |1d10        |
| wraith          |0        |0                |10    |55     |5     |4     |1     |1d6         |
| xeroc           |30       |0                |10    |100    |7     |7     |1     |4d4         |
| yeti            |30       |0                |10    |50     |4     |6     |1     |1d6+1d6     |
| zombie          |0        |mean             |10    |6      |2     |8     |1     |1d8         |

### monster special abilities

- hitting a xeroc identifies it (they mimic other monsters)
- phantoms are invisible
- Wraiths might drain energy levels
- Vampires can steal max HP
- venus_flytraps stop player from moving
- leprechauns steal gold
- nymphs steal magic items
- If the monster being attacked is not running (alseep or held),
  the attacker gets a plus four bonus to hit.

## potion

## strength
|str| +acc| +dmg|
|==================|
| 0  |-7   |-7|
| 1  |-6   |-6|
| 2  |-5   |-5|
| 3  |-4   |-4|
| 4  |-3   |-3|
| 5  |-2   |-2|
| 6  |-1   |-1|
| 7  | 0   | 0|
| 8  | 0   | 0|
| 9  | 0   | 0|
|10  | 0   | 0|
|11  | 0   | 0|
|12  | 0   | 0|
|13  | 0   | 0|
|14  | 0   | 0|
|15  | 0   | 0|
|16  | 0   | 1|
|17  | 1   | 1|
|18  | 1   | 2|
|19  | 1   | 3|
|20  | 1   | 3|
|21  | 2   | 4|
|22  | 2   | 5|
|23  | 2   | 5|
|24  | 2   | 5|
|25  | 2   | 5|
|26  | 2   | 5|
|27  | 2   | 5|
|28  | 2   | 5|
|29  | 2   | 5|
|30  | 2   | 5|
|31  | 3   | 6|

## traps

- trapdoor
- arrow trap
- sleeping gas trap
- beartrap
- teleport trap
- poison dart trap
- rust trap
- mysterious trap

## armor
|protection| name|
|===========|
|8| leather|
|7| ring_mail|
|7| studded_leather|
|6| scale_mail|
|5| chain_mail|
|4| splint_mail|
|4| banded_mail|
|3| plate_mail|

## xp levels
|level| xp needed|
|===========|
| 2 | 10 |
| 3 | 20 |
| 4 | 40 |
| 5 | 80 |
| 6 | 160 |
| 7 | 320 |
| 8 | 640 |
| 9 | 1300 |
| 10 | 2600 |
| 11 | 5200 |
| 12 | 13000 |
| 13 | 26000 |
| 14 | 50000 |
| 15 | 100000 |
| 16 | 200000 |
| 17 | 400000 |
| 18 | 800000 |
| 19 | 2000000 |
| 20 | 4000000 |
| 21 | 8000000 |

## scrolls
### messages

- "your hands begin to glow red": Scroll of monster confusion
- "the monsters around you freeze"/"you feel a strange sense of loss":
   Hold monster scroll.  Stop all monsters within two spaces
   from chasing after the hero.
- "you hear a faint cry of anguish in the distance":
  create monster
- "your nose tingles": smell food
- "you feel a strange sense of loss"/
  "your x glows blue for a moment": enchant weapon
- "you hear maniacal laughter in the distance":
  scare monsters
- "you feel in touch with the Universal Onenes"/"you feel as if somebody is watching over you"
  remove curse
- "you hear a high pitched humming noise":
   This scroll aggravates all the monsters on the current
   level and sets them running towards the hero
- "you feel a strange sense of loss"/
  "your armor is covered by a shimmering gold shield":
  armor protection

## potions
### messages

- "hey, this tastes great.  It make you feel warm all over":
  restore strength
- "you begin to feel better": heal



