## to do
### fix bugs
* ~~fix for big textfiles dir~~
* ~~breaks if <iframe> points to bad file~~
* ~~fix showdown strikethrough and tables~~
  * table:
    | Col 1   | Col 2 |
    |======== |====================================================|
    |**bold** | blah |
    | Plain   | ~~Value~~ |
* ~~"fix ReferenceError: reference to undefined property uls[(level - 2)]"~~
  * ~~happens when you navigate directly here: file:///.../wiki.html#todo~~
* ~~does not go to correct heading if you [jump pages](#jsnotes#Scope)~~
- fix broken tables ([see here](#RogueNotes))
- go to "404 page" if page is not found
- math doesn't render after navigating to a new page until you reload
- math doesn't render {}:
$$L = \{{{\bo x+t\bo v|t∈ℝ\}}}$$
$$\{\}`{$$
### core
* add jqmath
- take jquery out of jqmath
- need some way to get links to stuff
- lhs menu should be fixed, unless browser width is too small
  (then make it a bar at top)
- load fonts async, dnl if no innernet connection
- try out a bright and square metro look for the lhs menu
- history nav
- define index/frontpage somehow
- "go to top" button
* fix: third page doesn't display anymore (bad title, i think)
  * only opening paren gets removed (just allow parens in urls)
* markdown, or at least a simpler version of it
* WikiLinks
  - make some sorta @WikiLink shorthand?
- take out heading numbers in body
- loading
  - parse URL to go to correct page and heading
- test in older browsers
* fix warnings
* jslint the heck outta this thing
* rendering
  * markdown render
  * add headings
  * split into pages with regex
  * only render current page
  * only show current TOC (hide all others)
  * add pages list
  - show dummy page while markdown is loading?
  - float:right back to top button when hovering mouse over headings
- make it look okay
  * css
  * fix css: get style tags working
  - better looking css
* multiple pages (by h1???)
- history
  - back/fwd buttons should work
- saving
  - HTML5 file API
  - if no html5, fall back on: http://en.wikipedia.org/wiki/TiddlyWiki#File_saving
  - if none of that, don't allow editing or saving
- editing
  - use epiceditor? https://github.com/OscarGodson/EpicEditor
- put js utilities in small lib (or just use jquery, whatever man)
- compatibility
  - test in < ie9
    - need alternative to getElementsByClassName
- massive cleanup
  - needs to be READABLE and EXTENSIBLE
### ideas
- separate filename and wiki page name:
  - prefer <h1>, but use filename if there is no <h1>
### css/design
#### references:
* [twitter bootstrap](http://twitter.github.io/bootstrap/getting-started.html#download-bootstrap)
  * the left menu is pretty good.
* [wikipedia](http://en.wikipedia.org/wiki/Twitter_Bootstrap)
* [steam community guides](http://steamcommunity.com/sharedfiles/filedetails/?id=123364976)
  * the sidebar is really nice, especially how it automatically highlights the
    current section
### optional server
### images
- brainstorm an easy way to handle this
### config
- add config options:
  - show each page individually, or scrolling
  - draw pattern (fun!)
