/** 
* Get a randomized copy of the given array
*/
function shuffle(array) {
  var copy = array.slice();
  var deck = [];

  while (copy.length) {
    var i = Math.floor(Math.random() * copy.length);
    deck.push(copy.splice(i,1)[0])
  }

  return deck; 
}

/** 
* Get the keys of an object/dictionary
*/
function getKeys(object) {
  var keys = [];
  for (var key in object) {
    keys.push(key);
  }
  return keys;
}

var deck;
var numCards;
var currentCard;

function setupDeck() {
  var keys = getKeys(data.cards);
  deck = shuffle(keys);
  numCards = deck.length;
  deck.push("end.");
  nextCard();
}

function showCard(val) {
  var card = document.getElementById('flashcard');
  card.innerHTML = val;
}

function setProgressWidth() {
  var tofill = document.getElementById('tofill').offsetWidth;
  var progressBar = document.getElementById('progress');
  progress.style.width = (((numCards - deck.length) / numCards) * tofill) + "px";
}

function isCorrect(e) {
  var textfield = document.getElementById('answer');
  var userAnswer = textfield.value + String.fromCharCode(e.charCode);
  var correctAnswer = data.cards[currentCard];

  if (userAnswer == correctAnswer) {
    textfield.value = userAnswer;
    nextCard();
    textfield.focus();
    textfield.value = "";
    e.preventDefault();
  }
}

function nextCard() {
  if (deck.length) {
    var card = deck.shift();
    currentCard = card;
    showCard(card);
  }
  setProgressWidth();
}

window.onload = function() {
  setupDeck();

  var textfield = document.getElementById('answer');
  textfield.focus();

  //document.getElementById('button').onclick = isCorrect;
  //document.getElementById('answer').onchange = isCorrect;
  document.getElementById('answer').onkeypress = isCorrect;
}
